import { Component } from '@angular/core';
import { Events, NavController, LoadingController, Platform } from 'ionic-angular';
import { Dialogs } from '@ionic-native/dialogs';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { EventosProvider } from '../../providers/eventos/eventos';
import { EventosInfoPage } from '../eventos-info/eventos-info';

@Component({
  selector: 'eventos-page',
  templateUrl: 'eventos.html'
})
export class EventosPage {
  segmento: string;
  eventos: any;
  loading: any;
  str_sinConexion: string;

  constructor(
    public nav: NavController,
    public platform: Platform,
    public loadingCtrl: LoadingController,
    public events: Events,
    public eventosProvider: EventosProvider,
    private dialogs: Dialogs,
    private ga: GoogleAnalytics
  ) {
    let env = this;
    let token = localStorage.getItem('token');
    env.loading = env.loadingCtrl.create({ content: 'Cargando...' });
    env.str_sinConexion = 'Tu dispositivo tiene problemas de conexión.';
    env.segmento = 'hoy';

    // Verificar sesión del dispositivo
    env.events.publish('verificarSesion');

    env.showCargando();
    env.eventosProvider.getEventos(token)
      .subscribe(function(dataEventos) {
        env.hideCargando();
        console.log(dataEventos);
        if(dataEventos.status == 'ERROR') {
          env.alerta(dataEventos.error.message);
        } else if(dataEventos.status == 'OK') {
          env.eventos = dataEventos.data;
        }
      }, function(err) {
        env.hideCargando();
        env.alerta(env.str_sinConexion);
      });
  }

  ionViewDidLoad() {
    let env = this;
    env.ga.trackView('page-eventos');
  }

  showCargando() {
    let env = this;
    if(env.loading) {
      env.loading.present();
    } else {
      env.loading = env.loadingCtrl.create({ content: 'Cargando...' });
      env.loading.present();
    }
  }

  hideCargando() {
    let env = this;
    if(env.loading) {
      env.loading.dismiss();
      env.loading = null;
    }
  }

  alerta(mensaje: string) {
    let env = this;
    let titulo = 'Alerta';
    let boton = 'Aceptar';

    if(env.platform.is('cordova')) {
      env.dialogs.alert(mensaje, titulo, boton)
        .then(function () {
          console.log('Diálogo cerrado');
        })
        .catch(function (e) {
          console.log('Error al mostrar el diálogo', e);
        });
    } else {
      console.error(mensaje);
    }
  }

  abrirEvento(evento: any) {
    let env = this;
    env.nav.push(EventosInfoPage, { evento: evento });
  }
}