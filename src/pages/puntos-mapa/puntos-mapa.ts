import { Component, ElementRef, ViewChild } from '@angular/core';
import { LoadingController, NavParams, Platform } from 'ionic-angular';
import { Dialogs } from '@ionic-native/dialogs';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { Geolocation } from '@ionic-native/geolocation';
declare var google;

@Component({
  selector: 'page-puntos-mapa',
  templateUrl: 'puntos-mapa.html',
})
export class PuntosMapaPage {
  @ViewChild('mapContainer') mapContainer: ElementRef;
  map: any;
  punto: any;
  loading: any;

  constructor(
    public platform: Platform,
    public dialogs: Dialogs,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    private launchNavigator: LaunchNavigator,
    private geolocation: Geolocation,
    private ga: GoogleAnalytics
  ) {
    let env = this;
    env.loading = env.loadingCtrl.create({ content: 'Cargando...'});
    env.punto = env.navParams.get('punto');
  }

  ionViewDidLoad() {
    let env = this;
    env.platform.ready()
      .then(function() {
        env.platform.pause
          .subscribe(function() {
            env.hideCargando();
          });
      });
  }

  ionViewDidEnter() {
    let env = this;
    env.ga.trackView('page-puntos-mapa');
  }

  ionViewWillEnter() {
    let env = this;
    env.displayGoogleMap();
    env.addMarkerToMap(env.punto);
  }

  displayGoogleMap() {
    let env = this;
    let latLng = new google.maps.LatLng(env.punto.latitud, env.punto.longitud);

    let mapOptions = {
      center: latLng,
      zoom: 17,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    env.map = new google.maps.Map(env.mapContainer.nativeElement, mapOptions);
  }

  addMarkerToMap(marker) {
    var position = new google.maps.LatLng(marker.latitud, marker.longitud);
    var puntoMarker = new google.maps.Marker({ position: position, title: marker.name });
    puntoMarker.setMap(this.map);
  }

  showCargando() {
    let env = this;
    if(env.loading) {
      env.loading.present();
    } else {
      env.loading = env.loadingCtrl.create({ content: 'Cargando...' });
      env.loading.present();
    }
  }

  hideCargando() {
    let env = this;
    if(env.loading) {
      env.loading.dismiss();
      env.loading = null;
    }
  }

  alerta(message: string) {
    let env = this;
    let titulo = 'Alerta';
    let boton = 'Aceptar';
    env.dialogs.alert(message, titulo, boton)
      .then(function() { console.log('Diálogo cerrado'); })
      .catch(function(e) { console.log('Error al mostrar el diálogo', e); });
  }

  comoLlegar() {
    let env = this;
    // Cargando
    env.showCargando();
    env.geolocation.getCurrentPosition()
      .then(function(resp) {
        let options: LaunchNavigatorOptions = {
          start: [ resp.coords.latitude, resp.coords.longitude ]
        };

        env.launchNavigator.navigate([ env.punto.latitud, env.punto.longitud ], options)
          .then(function(success) {
            env.hideCargando();
            console.log('Navegador lanzado.');
          }, function(err) {
            console.log('Error al lanzar el navegador', err);
            env.hideCargando();
            env.alerta('Tu dispositivo no puede iniciar el navegador.');
          });
      })
      .catch(function(error) {
        console.log('Error al obtener la posición: ', error);
        console.log(error.code + ': ' + error.message);
        env.hideCargando();
        env.alerta('No se pudo obtener tu posición actual. Recuerda habilitar tu GPS.');
      });
  }
}