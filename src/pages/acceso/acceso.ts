import { Component } from '@angular/core';
import { NavController, LoadingController, Platform } from 'ionic-angular';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Dialogs } from '@ionic-native/dialogs';
import { AccesoProvider } from '../../providers/acceso/acceso';
import { PresentacionPage } from '../presentacion/presentacion';
import { TabsNavigationPage } from '../tabs-navigation/tabs-navigation';
import { GlobalVar } from '../../config';

@Component({
  selector: 'login-page',
  templateUrl: 'acceso.html'
})
export class AccesoPage {
  accederForm: FormGroup;
  main_page: { component: any };
  loading: any;
  str_sinConexion: string;

  constructor(
    public platform: Platform,
    private dialogs: Dialogs,
    public nav: NavController,
    public accesoProvider: AccesoProvider,
    public loadingCtrl: LoadingController,
  ) {
    let env = this;
    env.loading = env.loadingCtrl.create({ content: 'Cargando...' });
    env.main_page = { component: TabsNavigationPage };
    env.str_sinConexion = 'Tu dispositivo tiene problemas de conexión.';

    env.accederForm = new FormGroup({
      run: new FormControl('', [ Validators.required, Validators.minLength(4), Validators.maxLength(8) ]),
      contrasena: new FormControl('', Validators.required)
    });
  }

  showCargando() {
    let env = this;
    if(env.loading) {
      env.loading.present();
    } else {
      env.loading = env.loadingCtrl.create({ content: 'Cargando...' });
      env.loading.present();
    }
  }

  hideCargando() {
    let env = this;
    if(env.loading) {
      env.loading.dismiss();
      env.loading = null;
    }
  }

  olvidarContrasena() {
    window.open(GlobalVar.OLVIDAR_CONTRASENA, '_system');
  }

  presentacion() {
    let env = this;
    env.nav.setRoot(PresentacionPage);
    env.nav.popToRoot();
  }

  alerta(mensaje: string) {
    let env = this;
    let titulo = 'Alerta';
    let boton = 'Aceptar';

    if(env.platform.is('cordova')) {
      env.dialogs.alert(mensaje, titulo, boton)
        .then(function () {
          console.log('Diálogo cerrado');
        })
        .catch(function (e) {
          console.log('Error al mostrar el diálogo', e);
        });
    } else {
      console.error(mensaje);
    }
  }

  acceder() {
    let env = this;
    let run = env.accederForm.value.run;
    let contrasena = env.accederForm.value.contrasena;
    let dispositivo = localStorage.getItem('dispositivo') ? localStorage.getItem('dispositivo') : 'chrome';

    env.showCargando();
    env.accesoProvider.acceder(run, contrasena, dispositivo)
      .subscribe(function(dataLogin) {
        env.hideCargando();
        console.log('Acceso: ', dataLogin);
        if(dataLogin.status == 'ERROR') {
          let mensaje = dataLogin.error.message.replace('Invalid Credentials', 'Usuario o contraseña incorrectos');
          env.alerta(mensaje);
        } else if(dataLogin.status == 'OK') {
          let usuario = dataLogin.data;

          if(usuario.primerIngreso == 'S' || usuario.contrasenaCaducada == 'S') {
            // Enviar a portal de estudiante
            window.open(GlobalVar.PORTAL_ACADEMICO, '_system');
          } else {
            // Guardar datos personales
            localStorage.setItem('token', usuario.token);
            localStorage.setItem('run', usuario.run);
            localStorage.setItem('fotografia', usuario.fotografia);
            localStorage.setItem('movilidad', usuario.movilidad);
            localStorage.setItem('primerIngreso', usuario.primerIngreso);
            localStorage.setItem('contrasenaCaducada', usuario.contrasenaCaducada);
            localStorage.setItem('nombres', usuario.nombres);
            localStorage.setItem('apellidos', usuario.apellidos);
            localStorage.setItem('nombreCompleto', usuario.nombreCompleto);
            localStorage.setItem('email', usuario.email);
            localStorage.setItem('codPrograma', usuario.codPrograma);
            localStorage.setItem('carrera', usuario.carrera);
            localStorage.setItem('codCarrera', usuario.codCarrera);
            localStorage.setItem('numDecreto', usuario.numDecreto);
            localStorage.setItem('descEstado', usuario.descEstado);

            // Cambiar de página
            env.nav.setRoot(TabsNavigationPage);
            env.nav.popToRoot();
          }
        }
      }, function(err) {
        env.hideCargando();
        env.alerta(env.str_sinConexion);
      });
  }
}