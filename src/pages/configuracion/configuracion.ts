import { Component } from '@angular/core';
import { NavController, ModalController, LoadingController } from 'ionic-angular';
import { FormGroup, FormControl } from '@angular/forms';
import { PresentacionPage } from '../presentacion/presentacion';

@Component({
  selector: 'settings-page',
  templateUrl: 'configuracion.html'
})
export class ConfiguracionPage {
  settingsForm: FormGroup;
  // make PresentacionPage the root (or first) page
  rootPage: any = PresentacionPage;
  loading: any;
  perfil: any;

  constructor(
    public nav: NavController,
    public modal: ModalController,
    public loadingCtrl: LoadingController
  ) {
    let env = this;
    env.loading = env.loadingCtrl.create({ content: 'Cargando...' });

    env.settingsForm = new FormGroup({
      nombre: new FormControl(),
      pais: new FormControl(),
      cargo: new FormControl(),
      empresa: new FormControl(),
      biografia: new FormControl(),
      notificaciones: new FormControl()
    });
  }

  ionViewDidLoad() {
    let env = this;
    env.loading.present();
    env.settingsForm.setValue({
      nombre: localStorage.getItem('nombre'),
      pais: localStorage.getItem('pais'),
      cargo: localStorage.getItem('cargo'),
      empresa: localStorage.getItem('empresa'),
      biografia: localStorage.getItem('biografia'),
      notificaciones: localStorage.getItem('notficaciones')
    });
    env.loading.dismiss();
  }

  logout() {
    let env = this;
    // navigate to the new page if it is not the current page
    localStorage.clear();

    env.nav.push(env.rootPage);
  }
}
