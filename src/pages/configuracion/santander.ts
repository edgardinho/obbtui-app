import { AppAvailability } from '@ionic-native/app-availability';
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Market } from '@ionic-native/market';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@Component({
  selector: 'page-santander',
  template: ''
})
export class SantanderPage {

  constructor(
    private appAvailability: AppAvailability,
    private iab: InAppBrowser,
    private platform: Platform,
    private market: Market
  ) {
    let env = this,
      app, store;

    if(env.platform.is('ios')) {
      env.market.open('id604982236')
      /*
      app = 'santander-chile://';
      store = 'id604982236';
       */
    } else if(env.platform.is('android')) {
      env.market.open('cl.santander.smartphone');
      /*
      app = 'cl.santander.smartphone';
      store = app;
      */
    } else {
      console.log('Esta funcionalidad sólo funciona en dispositivos móviles.');
    }

    /*
    env.appAvailability.check(app)
      .then(function(yes: boolean) {
        let browser = env.iab.create(app, '_blank');
      }, function(no: boolean) {
        env.market.open(store);
        // window.open(store, '_blank', 'location=yes,enableViewPortScale=yes');
      });
      */
  }
}