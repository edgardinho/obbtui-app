import { Component } from '@angular/core';

import { NovedadesPage } from '../novedades/novedades';
import { PerfilPage } from '../perfil/perfil';
import { NotificacionesPage } from '../notificaciones/notificaciones';

@Component({
  selector: 'tabs-navigation',
  templateUrl: 'tabs-navigation.html'
})
export class TabsNavigationPage {
  tab1Root: any;
  tab2Root: any;
  tab3Root: any;
  id: string;

  constructor() {
    let env = this;
    env.id = localStorage.getItem('id');
    env.tab1Root = NovedadesPage;
    env.tab2Root = PerfilPage;
    env.tab3Root = NotificacionesPage;
  }
}
