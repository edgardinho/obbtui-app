import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { OneSignal } from '@ionic-native/onesignal';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { GlobalVar } from '../../config';

@Injectable()
export class NotificacionesProvider {
  url: string;

  constructor(
    private oneSignal: OneSignal,
    public platform: Platform,
    public http: HttpClient
  ) {
    let env = this;
    env.url = GlobalVar.BASE_API_URL + 'notificaciones/';
  }

  startNotificacionesPush() {
    let env = this;

    if(env.platform.is('cordova')) {
      console.log('Cargando notificaciones push');
      env.oneSignal.startInit(GlobalVar.ONESIGNAL_ID, GlobalVar.FIREBASE_PID);
      env.oneSignal.inFocusDisplaying(env.oneSignal.OSInFocusDisplayOption.InAppAlert);
      env.oneSignal.handleNotificationReceived()
        .subscribe(function() {
          console.log('Notificación recibida.');
        });

      env.oneSignal.handleNotificationOpened()
        .subscribe(function() {
          console.log('Notificación abierta.');
        });

      env.oneSignal.getIds()
        .then(function(data) {
          console.log('userId', data.userId);
          localStorage.setItem('dispositivo', data.userId);
        })
        .catch(function(err) {
          console.log('Error 2', err);
        });

      env.oneSignal.endInit();
    } else {
      console.log('Notificaciones push no funcionan en navegadores.');
    }
  }

  getNotificaciones(token: string): Observable<any> {
    let env = this;
    let headers = { headers: new HttpHeaders().set('Authorization', token) };
    return env.http
      .post(env.url, {}, headers)
      .map(function(response) { return response })
      .catch(env.handleError);

  }

  private handleError(error: any): Promise<any> {
    console.error('Ha ocurrido un error.', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}
