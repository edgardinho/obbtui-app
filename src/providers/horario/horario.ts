import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { GlobalVar } from '../../config';

@Injectable()
export class HorarioProvider {
  url_horario: string;
  url_bloques: string;

  constructor(public http: HttpClient) {
    let env = this;
    env.url_horario = GlobalVar.BASE_API_URL + 'horario';
    env.url_bloques = GlobalVar.BASE_API_URL + 'horario/bloques';
  }

  getHorario(token: string): Observable<any> {
    let env = this;
    let headers = { headers: new HttpHeaders().set('Authorization', token) };
    return env.http
      .post(env.url_horario, {}, headers)
      .map(function (response) { return response })
      .catch(env.handleError);
  }

  getBloques(token: string): Observable<any> {
    let env = this;
    let headers = { headers: new HttpHeaders().set('Authorization', token) };
    return env.http
      .post(env.url_bloques, {}, headers)
      .map(function (response) { return response })
      .catch(env.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('Ha ocurrido un error', error);
    return Promise.reject(error.message || error);
  }
}