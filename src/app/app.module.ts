import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { IonicApp, IonicModule } from 'ionic-angular';
import { Dialogs } from '@ionic-native/dialogs';
import { OneSignal } from '@ionic-native/onesignal';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { MyApp } from './app.component';

// Páginas
import { AccesoPage } from '../pages/acceso/acceso';
import { PresentacionPage } from '../pages/presentacion/presentacion';
import { NovedadesPage } from '../pages/novedades/novedades';
import { NotificacionesPage } from '../pages/notificaciones/notificaciones';
import { PerfilPage } from '../pages/perfil/perfil';
import { TabsNavigationPage } from '../pages/tabs-navigation/tabs-navigation';
import { ConfiguracionPage } from '../pages/configuracion/configuracion';
import { CredencialPage } from '../pages/credencial/credencial';
import { EventosPage } from '../pages/eventos/eventos';
import { EventosInfoPage } from '../pages/eventos-info/eventos-info';
import { AsignaturasPage } from '../pages/asignaturas/asignaturas';
import { HorarioPage } from '../pages/horario/horario';
import { EnlacesPage } from '../pages/enlaces/enlaces';
import { PuntosPage } from '../pages/puntos/puntos';
import { PuntosMapaPage } from '../pages/puntos-mapa/puntos-mapa';
import { DeudasPage } from '../pages/deudas/deudas';
import { SantanderPage } from '../pages/configuracion/santander';
import { InformacionPage } from '../pages/informacion/informacion';
import { SalirPage } from '../pages/configuracion/salir';

// Componentes
import { ComponentsModule } from '../components/components.module';
import { PipesModule } from '../pipes/pipes.module';

// Modulos
import { QRCodeModule } from 'angular2-qrcode';

// Proveedores
import { NotificacionesProvider } from '../providers/notificaciones/notificaciones';
import { AccesoProvider } from '../providers/acceso/acceso';
import { NovedadesProvider } from '../providers/novedades/novedades';
import { EventosProvider } from '../providers/eventos/eventos';
import { AsignaturasProvider } from '../providers/asignaturas/asignaturas';
import { HorarioProvider } from '../providers/horario/horario';
import { EnlacesProvider } from '../providers/enlaces/enlaces';
import { PuntosProvider } from '../providers/puntos/puntos';
import { DeudasProvider } from '../providers/deudas/deudas';
import { PortalAcademicoProvider } from '../providers/portal-academico/portal-academico';
import { AulaVirtualProvider } from '../providers/aula-virtual/aula-virtual';
import { PeriodoProvider } from '../providers/periodo/periodo';
import { SesionProvider } from '../providers/sesion/sesion';

// Plugins
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { BackgroundMode } from '@ionic-native/background-mode';
import { CallNumber } from '@ionic-native/call-number';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { Geolocation } from '@ionic-native/geolocation';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { AppVersion } from '@ionic-native/app-version';
import { Market } from '@ionic-native/market';
import { AppAvailability } from '@ionic-native/app-availability';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@NgModule({
  declarations: [
    MyApp,
    AccesoPage,
    PresentacionPage,
    NovedadesPage,
    PerfilPage,
    NotificacionesPage,
    TabsNavigationPage,
    ConfiguracionPage,
    SalirPage,
    CredencialPage,
    EventosPage,
    EventosInfoPage,
    AsignaturasPage,
    HorarioPage,
    EnlacesPage,
    PuntosPage,
    PuntosMapaPage,
    InformacionPage,
    DeudasPage,
    SantanderPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    ComponentsModule,
    PipesModule,
    QRCodeModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: 'Atrás'
    })
  ],
  bootstrap: [ IonicApp ],
  entryComponents: [
    MyApp,
    AccesoPage,
    PresentacionPage,
    NovedadesPage,
    PerfilPage,
    NotificacionesPage,
    TabsNavigationPage,
    ConfiguracionPage,
    SalirPage,
    CredencialPage,
    EventosPage,
    EventosInfoPage,
    AsignaturasPage,
    HorarioPage,
    EnlacesPage,
    PuntosPage,
    PuntosMapaPage,
    InformacionPage,
    DeudasPage,
    SantanderPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Dialogs,
    BackgroundMode,
    GoogleAnalytics,
    LocalNotifications,
    LaunchNavigator,
    Geolocation,
    OneSignal,
    CallNumber,
    AppVersion,
    AppAvailability,
    Market,
    InAppBrowser,

    NotificacionesProvider,
    AccesoProvider,
    NovedadesProvider,
    EventosProvider,
    AsignaturasProvider,
    HorarioProvider,
    EnlacesProvider,
    PuntosProvider,
    DeudasProvider,
    PortalAcademicoProvider,
    AulaVirtualProvider,
    PeriodoProvider,
    SesionProvider

  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule {}