import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav, App, Events, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Dialogs } from '@ionic-native/dialogs';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { PresentacionPage } from '../pages/presentacion/presentacion';
import { AccesoPage } from '../pages/acceso/acceso';
import { TabsNavigationPage } from '../pages/tabs-navigation/tabs-navigation';
import { SesionProvider } from '../providers/sesion/sesion';
import { NotificacionesProvider } from '../providers/notificaciones/notificaciones';
import { CredencialPage } from '../pages/credencial/credencial';
import { EventosPage } from '../pages/eventos/eventos';
import { AsignaturasPage } from '../pages/asignaturas/asignaturas';
import { HorarioPage } from '../pages/horario/horario';
import { EnlacesPage } from '../pages/enlaces/enlaces';
import { PuntosPage } from '../pages/puntos/puntos';
import { DeudasPage } from '../pages/deudas/deudas';
import { SantanderPage } from '../pages/configuracion/santander';
import { InformacionPage } from '../pages/informacion/informacion';
import { SalirPage } from '../pages/configuracion/salir';
import { GlobalVar } from '../config';

@Component({
  selector: 'app-root',
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = localStorage.getItem('token') ? TabsNavigationPage :
    localStorage.getItem('presentacion') == null ? PresentacionPage : AccesoPage;
  pages: Array<{ title: string, icon: string, component: any }>;
  pushPages: Array<{ title: string, icon: string, component: any }>;
  loading: any;
  str_sinConexion: string;

  constructor(
    public platform: Platform,
    public menu: MenuController,
    public app: App,
    public dialogs: Dialogs,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public loadingCtrl: LoadingController,
    public events: Events,
    public notificacionesProvider: NotificacionesProvider,
    public sesionProvider: SesionProvider,
    private ga: GoogleAnalytics
  ) {
    let env = this;
    let token = localStorage.getItem('token') || 'sinToken';
    env.str_sinConexion = 'Tu dispositivo tiene problemas de conexión.';

    events.subscribe('verificarSesion', function() {
      let token = localStorage.getItem('token') || 'sinToken';
      env.verificarSesion(token);
    });

    platform.ready().then(function() {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      env.splashScreen.hide();
      env.statusBar.styleDefault();

      if(!platform.is('cordova')) {
        console.log('Google Analytics no funciona en navegadores.');
      } else {
        // Cargar Google Analytics
        env.ga.startTrackerWithId(GlobalVar.GANALYTICS_API_KEY)
          .then(function() {
            console.log('Google Analytics está listo.');
          })
          .catch(function(e) {
            console.log('Error al cargar Google Analytics', e);
          });
      }

      // Cargar notificaciones push
      // env.notificacionesProvider.startNotificacionesPush();
    });

    // Verificar sesión del dispositivo si existe
    if(localStorage.getItem('token') != null) {
      // env.verificarSesion(token);
    }

    env.pages = [
      { title: 'Inicio', icon: 'home', component: TabsNavigationPage },
      // { title: 'Mi TUI', icon: 'card', component: CredencialPage },
      // { title: 'Eventos', icon: 'calendar', component: EventosPage },
      // { title: 'Asignaturas', icon: 'book', component: AsignaturasPage },
      // { title: 'Horario', icon: 'time', component: HorarioPage },
      // { title: 'Enlaces', icon: 'link', component: EnlacesPage },
      // { title: 'Puntos de interés', icon: 'pin', component: PuntosPage },
      // { title: 'Saldos', icon: 'cash', component: DeudasPage },
      { title: 'Banco Santander', icon: 'phone-portrait', component: SantanderPage },
      { title: 'Información', icon: 'information-circle', component: InformacionPage },
      { title: 'Salir', icon: 'log-out', component: SalirPage }
    ];

    env.pushPages = [];
  }

  openPage(page) {
    let env = this;
    env.menu.close();
    env.nav.setRoot(page.component);
  }

  pushPage(page) {
    let env = this;
    // close the menu when clicking a link from the menu
    env.menu.close();
    // rootNav is now deprecated (since beta 11) (https://forum.ionicframework.com/t/cant-access-rootnav-after-upgrade-to-beta-11/59889)
    env.app.getRootNav().push(page.component);
  }

  showCargando() {
    let env = this;
    if(env.loading) {
      env.loading.present();
    } else {
      env.loading = env.loadingCtrl.create({ content: 'Cargando...' });
      env.loading.present();
    }
  }

  hideCargando() {
    let env = this;
    if(env.loading) {
      env.loading.dismiss();
      env.loading = null;
    }
  }

  alerta(mensaje: string) {
    let env = this;
    let titulo = 'Alerta';
    let boton = 'Aceptar';

    if(env.platform.is('cordova')) {
      env.dialogs.alert(mensaje, titulo, boton)
        .then(function () {
          console.log('Diálogo cerrado');
        })
        .catch(function (e) {
          console.log('Error al mostrar el diálogo', e);
        });
    } else {
      console.error(mensaje);
    }
  }

  verificarSesion(token: string) {
    let env = this;

    env.sesionProvider.verificar(token)
      .subscribe(function(dataSesion) {
        console.log(dataSesion);
        if(dataSesion.hasOwnProperty('data')) {
          if(!dataSesion.data.valida) {
            // Limpiar sesión y mover a la página de acceso
            console.log('Sesión no válida.');
            localStorage.clear();
            localStorage.setItem('presentacion', 'false');
            env.nav.setRoot(AccesoPage);
            env.nav.popToRoot();
          } else {
            console.log('Sesión válida.');
          }
        } else {
          env.alerta(dataSesion.error.message);
        }
      }, function(err) {
        env.alerta(env.str_sinConexion);
      });
  }
}